/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m15.pe1.newadnmanager;



/**
 *
 * @author paco
 */
public class AdnTeclat {

    //Atributs:
    private String cadena ="";
    
    
    //Constructors:
    public AdnTeclat(String cadena) {
        this.cadena= cadena;
    }
    //Getters & Setters:
    public String getCadena() {
        return cadena;
    }

    public void setCadena(String cadena) {
        this.cadena = cadena;
    }
    
    /**
     * Retorna la cadena d'adn en la seva versió invertida
     * @return L'atribut cadena invertit
     */
    public String invertida(){
        StringBuilder sb = new StringBuilder(this.cadena);
        return sb.reverse().toString();
    }
    
    /**
     * Calcula la cadena complementaria a partir de la cadena inicial Substituïnt:
     * A->T
     * T->A
     * C->G
     * G->A
     * Si la lletra no pertany a un ADN ho substituirà per "/"
     * @return Retorna cada una de les substitucions convertides en array- 
     */
    public String complementaria(){
        StringBuilder sb = new StringBuilder();
        for(int i =0; i<this.cadena.length(); i++){
            if(this.cadena.toUpperCase().charAt(i) == 'A'){
                sb.append('T');
            }else if(this.cadena.toUpperCase().charAt(i)== 'T'){
                 sb.append('A');
            }else if(this.cadena.toUpperCase().charAt(i) == 'G'){
                 sb.append('C');
            }else if(this.cadena.toUpperCase().charAt(i) == 'C'){
                 sb.append('G');
            }else{
                sb.append("/");
            }
        }
        return sb.toString();
    }
    
    
    
    
    
    
    
}
