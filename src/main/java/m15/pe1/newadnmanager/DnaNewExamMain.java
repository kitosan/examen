package m15.pe1.newadnmanager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author tarda
 */
public class DnaNewExamMain {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    
    static final String TOTAL_DE = "Total de";
    static final String PERCENT = "Percentatge";
    static final String A = "Adenina";
    static final String C = "Citosina";
    static final String G = "Guanina";
    static final String T = "Timina";
    
    
    public static void main(String[] args) throws IOException {
        Scanner intro = new Scanner(System.in);
        System.out.println("Si us plau, escriu la cadena d'adn...");
        AdnTeclat strand = new AdnTeclat(intro.next());
        
        System.out.println("Cadena ADN introduida:");
        System.out.println(strand.toString());
        System.out.println("La seva cadena complementaria es : ");
        System.out.println(strand.complementaria());
        
        
        DnaNewExamMain myApp = new DnaNewExamMain();
        myApp.run();
    }

    /**
     * Function that runs the app
     */
    private void run() {
        int option = 0;
        ADN_Manager cadenaADN = new ADN_Manager();
        ADNFileReader file = new ADNFileReader();
        ArrayList<String> dnaSequence_list = file.readSequence("src/main/java/m15/pe1/newadnmanager/dnaSequence.txt");
        String dnaSequence = String.join("", dnaSequence_list);
        dnaSequence = dnaSequence.toUpperCase();

        do {
            showMenu();
            Scanner myScan = new Scanner(System.in);
            option = myScan.nextInt();
            myScan.nextLine();
            
            switch (option) {
                case 0:
                    System.out.println("Bye!");
                    break;
                case 1:
                    System.out.println("**Donar la volta "+dnaSequence+"**");
                    System.out.println("");
                    System.out.println(cadenaADN.invertADN(dnaSequence));
                    break;
                case 2:
                    System.out.println("**Trobar la base més repetida "+dnaSequence+"**");
                    System.out.println("");
                    System.out.println(cadenaADN.maxLetter(dnaSequence));
                    break;
                case 3:
                    System.out.println("**Trobar la base menys repetida "+dnaSequence+"**");
                    System.out.println("");
                    System.out.println(cadenaADN.minLetter(dnaSequence));
                    break;
                case 4:
                    System.out.println("**Fer recompte de bases "+dnaSequence+"**");
                    System.out.println("");
                    System.out.println(TOTAL_DE + " A: "+cadenaADN.numAdenines(dnaSequence));
                    System.out.println(TOTAL_DE + " C: "+cadenaADN.numCitosines(dnaSequence));
                    System.out.println(TOTAL_DE + " G: "+cadenaADN.numGuanines(dnaSequence));
                    System.out.println(TOTAL_DE + " T: "+cadenaADN.numTimines(dnaSequence));
                    break;
                case 5: 
                    System.out.println("Calcul de percentatges: ");
                    System.out.println("");
                    System.out.println(dnaSequence);
                    System.out.println(PERCENT + A + cadenaADN.percentatgeAdenines(dnaSequence));
                    System.out.println(PERCENT + C +cadenaADN.percentatgeCitosines(dnaSequence));
                    System.out.println(PERCENT + G +cadenaADN.percentatgeGuanines(dnaSequence));
                    System.out.println(PERCENT + T +cadenaADN.percentatgeTimines(dnaSequence));
                    break;
                default:
                    System.out.println("La opció seleccionada no és válida.");
                    break;
            }
        } while (option != 0);
    }

    public void showMenu() {
        System.out.println("\n Tria una opció:");
        System.out.println("0.-Sortir");
        System.out.println("1.-Donar la volta\n"
                + "2.- Trobar la base més repetida,\n"
                + "3.- Trobar la base menys repetida\n"
                + "4.- Fer recompte de bases\n"
                + "5.- Calcul percentatges");
        System.out.print("\nOpció: ");
    }

    private void ShowSecuences(String dnaSequence, String dnaSubSequence) {
        System.out.println("DNA sequence: " + dnaSequence);
        System.out.println("DNA subSequence: " + dnaSubSequence);
    }
}
