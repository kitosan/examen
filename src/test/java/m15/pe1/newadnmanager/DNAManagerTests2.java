/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m15.pe1.newadnmanager;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author tarda
 */
public class DNAManagerTests2 {

    String dnaSequence;
    ADN_Manager dnaFunct;

    public DNAManagerTests2() {
        dnaSequence = "GATATGC";
        dnaFunct = new ADN_Manager();
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    @Test
    public void testMinLetterDNA_COneTime() {
        dnaSequence = "GATATGC";
        String expected = "C";
        String result = dnaFunct.minLetter(dnaSequence);
        assertEquals(result, expected);
    }

    @Test
    public void testMinLetterDNA_T_Twice() {
        dnaSequence = "CCGATACATGAC";
        String expected = "T";
        String result = dnaFunct.minLetter(dnaSequence);
        assertEquals(result, expected);
    }

    @Test
    public void testpercentatgeAdenines() {
        String ADN = "CGTCGTCGT";
        String ADN2 = "AAAAAaaaAAAAAA";
        String ADN3 = "acgt";
        ADN_Manager cadena = new ADN_Manager();
        assertEquals(cadena.percentatgeAdenines(ADN), 0);
        assertEquals(cadena.percentatgeAdenines(ADN2), 100);
        assertEquals(cadena.percentatgeAdenines(ADN3), 0.25);
    }

    @Test
    public void testRecompteAdenines() {
        String ADN = "CGTCGTCGT";
        String ADN2 = "AAAAAAAAAAA";
        String ADN3 = "acgtacgtaaacgttttgcaaacgt";
        ADN_Manager cadena = new ADN_Manager();
        assertEquals(cadena.numAdenines(ADN), 0);
        assertEquals(cadena.numAdenines(ADN2), 11);
        assertEquals(cadena.numAdenines(ADN3), 8);
    }
    
    @Test
    public void testmaxLetter(){
        String ADN = "CGTCGTCGT";
        String ADN2 = "AttAAAAaaaAAAcAAA";
        String ADN3 = "acgtacgtacgt";
        ADN_Manager cadena = new ADN_Manager();
        assertEquals(cadena.maxLetter(ADN), "T");   /**Aquesta funcio esta programada amb un "else" per tant si hiha més d'una lletra amb elmaxim nombre respondrà amb la "T"*/
        assertEquals(cadena.maxLetter(ADN2), "A");
        assertEquals(cadena.maxLetter(ADN3), "T");
    }
}
